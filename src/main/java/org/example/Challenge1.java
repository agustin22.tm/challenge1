package org.example;

import java.util.Scanner;
import java.lang.Math;


public class Challenge1 {
    public static void main(String[] args)  {
        mainMenu();
    }

    // Fungsi untuk tampilan Main Menu
    public static void mainMenu(){
        System.out.println("---------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("---------------------------------------");
        Scanner inputMenu = new Scanner(System.in);
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume");
        System.out.println("0. Tutup Aplikasi");
        System.out.print("Masukkan option : ");
        int inputMenu2 =  inputMenu.nextInt();

        // Logic untuk memilih pilihan pada Main Menu
        switch (inputMenu2){
            case(1) : areaMenu();
                break;
            case(2) : volumeMenu();
                break;
            case(0): stopProgram();
                break;
        }
    }
    // Fungsi untuk tampilan Area Menu
    public static void areaMenu(){
        System.out.println("-------------");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("-------------");

        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. kembali ke menu sebelumnya");

        Scanner area = new Scanner(System.in);
        System.out.print("Masukkan option : ");
        int areaOption = area.nextInt();

        // Logic untuk memilih pilihan pada Area Menu
        switch (areaOption) {
            case (1): squareArea();
                break;
            case (2): circleArea();
                break;
            case (3): triangleArea();
                break;
            case (4): rectangleArea();
                break;
            case (0): mainMenu();
                break;
        }
    }
    // Fungsi untuk tampilan Volume Menu
    public static void volumeMenu(){
        System.out.println("--------------");
        System.out.println("Pilih ruang yang akan dihitung");
        System.out.println("--------------");

        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");

        Scanner volume = new Scanner(System.in);
        int volumeOption = volume.nextInt();

        // Logic untuk memilih pilihan pada Volume Menu
        switch (volumeOption) {
            case (1): cubeVolume();
                break;
            case (2): cuboidVolume();
                break;
            case (3): tubeVolume();
                break;
            case (0): mainMenu();
                break;
        }
    }

    // Fungsi untuk kembali ke Main Menu
    public static void backToMainMenu(){
        /// Logic untuk kembali ke menu utama
        Scanner input = new Scanner(System.in);
        String input2 = input.next();
        if (input2 != null){
            mainMenu();
        }
    }

    // Fungsi untuk menghentikan program
    public static void stopProgram(){
        System.out.println("Program telah berhenti");
    }

    // Fungsi untuk menghitung luas Persegi
    public static void squareArea(){
        System.out.println("-------------");
        System.out.println("Anda memilih persegi");
        System.out.println("-------------");

        System.out.print("Masukkan panjang : ");
        Scanner baseInput = new Scanner(System.in);
        double baseInput2 = baseInput.nextDouble();

        // Fungsi untuk rumus luas Persegi
        double areaFormula = Math.pow(baseInput2, 2);

        double result = areaFormula;

        System.out.println("Processing...");
        System.out.println();
        System.out.println("Luas dari Persegi adalah " + result);
        System.out.print("Tekan apa saja untuk kembali ke menu utama : ");

        backToMainMenu();
    }

    // Fungsi untuk menghitung luas Lingkaran
    public static void circleArea(){
        System.out.println("-----------");
        System.out.println("Anda memilih lingkaran");
        System.out.println("-----------");

        System.out.print("Masukkan jari-jari : ");
        Scanner radius = new Scanner(System.in);
        double radius2 = radius.nextDouble();

        // Fungsi untuk rumus luas Lingkaran
        double pi = Math.PI;
        double radiusSquare = Math.pow(radius2, 2);
        double result = pi * radiusSquare;

        System.out.println("Processing...");
        System.out.println();
        System.out.println("Luas dari Lingkaran adalah "+ result);
        System.out.println("---------------");
        System.out.print("Tekan apa saja untuk kembali ke menu utama : ");

        backToMainMenu();
    }

    // Fungsi untuk menghitung luas Segitiga
    public static void triangleArea(){
        System.out.println("------------");
        System.out.println("Anda memilih segitiga");
        System.out.println("------------");

        System.out.print("Masukkan alas : ");
        Scanner baseInput = new Scanner(System.in);
        double baseInput2 = baseInput.nextInt();

        System.out.print("Masukkan tinggi : ");
        Scanner heightInput = new Scanner(System.in);
        double heightInput2 = heightInput.nextInt();

        // Fungsi untuk rumus luas segitiga
        double half = 0.5;
        double result = half * baseInput2 * heightInput2;

        System.out.println("Processing...");
        System.out.println();
        System.out.println("Luas dari segitiga adalah " + result);
        System.out.println("----------------------------");
        System.out.print("Tekan apa saja untuk kembali ke menu utama : ");

        backToMainMenu();
    }

    // Fungsi untuk menghitung luas Persegi Panjang
    public static void rectangleArea(){
        System.out.println("---------");
        System.out.println("Anda memilih persegi panjang");
        System.out.println("----------");

        System.out.print("Masukkan panjang : ");
        Scanner lengthInput = new Scanner(System.in);
        int lengthInput2 = lengthInput.nextInt();

        System.out.print("Masukkan lebar : ");
        Scanner widthInput = new Scanner(System.in);
        int widthInput2 = widthInput.nextInt();

        // Fungsi untuk rumus luas persegi panjang
        int result = lengthInput2 * widthInput2;

        System.out.println("Processing...");
        System.out.println();
        System.out.println("Luas dari persegi panjang adalah " + result);
        System.out.println("----------------------------");
        System.out.print("Tekan apa saja untuk kembali ke menu utama : ");

        backToMainMenu();
    }

    // Fungsi untuk menghitung volume Tabung
    public static void tubeVolume(){
        System.out.println("---------");
        System.out.println("Anda memilih Tabung");
        System.out.println("----------");

        System.out.print("Masukkan jari-jari : ");
        Scanner radius = new Scanner(System.in);
        double radius2 = radius.nextDouble();

        System.out.print("Masukkan tinggi : ");
        Scanner height = new Scanner(System.in);
        double height2 = height.nextDouble();

        // Fungsi untuk rumus volume Tabung
        double piConstant = Math.PI;
        double radiusSquare = Math.pow(radius2, 2);

        double result = piConstant * radiusSquare * height2;

        System.out.println("Procesing...");
        System.out.println();
        System.out.println("Volume dari Tabung adalah : " + result);
        System.out.println("----------------");
        System.out.print("Tekan apa saja untuk kembali ke menu utama : ");

        backToMainMenu();

    }

    // Fungsi untuk menghitung Volume Balok
    public static void cuboidVolume(){
        System.out.println("----------");
        System.out.println("Anda memilih Balok");
        System.out.println("----------");

        System.out.print("Masukkan panjang : ");
        Scanner lengthInput = new Scanner(System.in);
        int lengthInput2 = lengthInput.nextInt();

        System.out.print("Masukkan lebar : ");
        Scanner widthInput = new Scanner(System.in);
        int widthInput2 = widthInput.nextInt();

        System.out.print("Masukkan tinggi : ");
        Scanner heightInput = new Scanner(System.in);
        int heightInput2 = heightInput.nextInt();

        // Fungsi untuk rumus volume Balok
        int result = lengthInput2 * widthInput2 * heightInput2;

        System.out.println("Processing...");
        System.out.println();
        System.out.println("Volume dari Balok adalah = " + result);
        System.out.println("-----------------");
        System.out.println("Tekan apa saja untuk kembali ke menu utama : ");

        backToMainMenu();
    }

    // Fungsi menghitung volume Kubus
    public static void cubeVolume(){
        System.out.println("----------------");
        System.out.println("Anda memilih kubus");
        System.out.println("----------------");

        System.out.print("Masukkan panjang : " );
        Scanner lengthInput = new Scanner(System.in);
        double lengthInput2 = lengthInput.nextDouble();

        // Fungsi untuk rumus volume Kubus
        double volumeFormula = Math.pow(lengthInput2, 3);

        Double result = volumeFormula;

        System.out.println("Processing...");
        System.out.println();
        System.out.println("Volume dari Kubus adalah " + result);
        System.out.println("--------------------");
        System.out.print("Tekan apa saja untuk kembali ke menu utama : ");

        backToMainMenu();
    }
}
